import Component from '@glimmer/component';
import { tracked } from '@glimmer/tracking';
import { action } from '@ember/object';
import { service } from '@ember/service';
import { computed } from '@ember/object';

export default class TestComponentComponent extends Component {

  @service router;

  @tracked qno = 1;

  @tracked userAnswer = null;

  @tracked scoreArray = [];

  @tracked ansArray = [];

  @tracked count = 0;

  @tracked interval = "";

  @tracked selected = "";

  @tracked showSelected = false;

  @tracked qRoute = this.args.questionRoute;

  @action next() {
    this.showSelected = false;
    let answer = this.model.answer;
    if (this.qno <= 5) {
      if (this.userAnswer != null) {
        if (answer === this.userAnswer) {
          this.scoreArray[this.qno] = 2;
          this.ansArray[this.qno] = this.userAnswer;
        } else {
          this.scoreArray[this.qno] = -1;
          this.ansArray[this.qno] = this.userAnswer;
        }
        this.changeQues();
        this.userAnswer = null;
      } else {
        alert('select any answer');
      }
    }
  }
  
  @action previous() {
    this.qno = this.qno - 1;
    this.showSelected = true;
    this.selected = this.ansArray[this.qno];
    this.router.transitionTo("/" + this.qRoute + "/" + this.qno);
  }

  @action toHome() {
    this.resetValues();
    this.router.transitionTo('index');
  }

  @action userChoice(args) {
    this.userAnswer = args;
  }

  get model() {
    return this.args.model;
  }

  @action quizTimer() {
    this.count = 20;
    this.interval = setInterval(() => {
      this.count--;
      if (this.count === 0) {
        clearInterval(this.interval);
        this.qno = 'result';
        this.router.transitionTo("/" + this.qRoute + "/result");
      }
    }, 1000);
  }

  @computed("qno") 
  get btnValue(){
    if(this.qno < 5){
      return "next";
    }
    return "submit";
  }

  @action changeQues(){   
    if(this.qno < 5){
      this.qno++;
      this.router.transitionTo("/" + this.qRoute + "/" + this.qno);
    }
    else{
      this.qno = "result";
      clearInterval(this.interval);
      this.router.transitionTo("/" + this.qRoute + "/result");
    }
  }

  get score(){
    let temp = 0;
    this.scoreArray.forEach(element => {
      temp = temp + element;
    });
    return temp;
  }

  @action resetValues(){
    this.userAnswer = null;
    this.scoreArray = [];
    this.ansArray = [];
    this.interval = "";
    this.selected = "";
    this.count = 0;
    this.qno = 1;
  }

}