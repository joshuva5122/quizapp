import Model, { attr } from '@ember-data/model';

export default class QuizModel extends Model {
  @attr questionType;
  @attr questionNo;
  @attr question;
  @attr answer;
  @attr choice;
}
