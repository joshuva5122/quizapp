import EmberRouter from '@ember/routing/router';
import config from 'quiz-app/config/environment';

export default class Router extends EmberRouter {
  location = config.locationType;
  rootURL = config.rootURL;
}

Router.map(function () {
  this.route('Trivia', { path: 'Trivia/:qno' });
  this.route('Physics', { path: 'Physics/:qno' });
  this.route('Economics', { path: 'Economics/:qno' });
  this.route('Mathametics', { path: 'Mathametics/:qno' });
  this.route('not-found', { path: '/*' });
});
