import Route from '@ember/routing/route';
import { service } from '@ember/service';

export default class MathameticsRoute extends Route {
  @service store;

  async beforeModel() {
    let response = await fetch('/quiz.json');
    return response.json().then((resp) => {
      this.store.pushPayload({ quiz: resp.quiz.mathametics });
    });
  }

  model(params) {
    let { qno } = params;
    if (parseInt(qno) >= 1 && parseInt(qno) <= 5) {
      return this.store.peekAll('quiz').find((element) => {
        return element.questionType === 'mathametics' && element.questionNo === qno;
      });
    }
  }
}
